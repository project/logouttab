<?php

namespace Drupal\Tests\logouttab\Functional;

use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;

/**
 * Tests the module functionality.
 *
 * @group logouttab
 */
class LogoutTabTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['block', 'logouttab', 'user'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Tests that the module actually works.
   */
  public function testBaseSettings() {
    $block = strtolower($this->randomMachineName());
    $this->drupalPlaceBlock('local_tasks_block', ['id' => $block]);
    $settings = '/admin/config/people/logouttab';
    $this->drupalGet($settings);
    $this->assertSession()->statusCodeEquals(403);
    $user = $this->createUser(['administer users']);
    $this->drupalLogin($user);
    $tabs = $this->cssSelect("#block-{$block} ul li a");
    $this->assertCount(3, $tabs);
    $this->assertSame('Log out', $tabs[2]->getText());

    // Test default settings.
    $this->drupalGet($settings);
    $session = $this->assertSession();
    $session->statusCodeEquals(200);
    $element = $this->cssSelect('input[name="url"]');
    $this->assertSame('user/logout', $element[0]->getValue());
    $element = $this->cssSelect('select[name="weight"]');
    $this->assertSame('8', $element[0]->getValue());
    // Check field prefix works.
    $session->responseContains(Url::fromRoute('<front>', [], ['absolute' => TRUE])->toString());
    // Check cacheability works.
    $edit = ['weight' => -30];
    $this->submitForm($edit, 'Save configuration');
    $element = $this->cssSelect('select[name="weight"]');
    $this->assertSame('-30', $element[0]->getValue());
    $this->assertSame(-30, \Drupal::config('logouttab.settings')->get('weight'));
    // Check the tab displayed first.
    $this->drupalGet($user->toUrl());
    $tabs = $this->cssSelect("#block-{$block} ul li a");
    $this->assertCount(3, $tabs);
    $this->assertSame('Log out', $tabs[0]->getText());
    // Check tabs are not visible for other users.
    $this->drupalLogin($this->createUser(['access user profiles']));
    $this->drupalGet($user->toUrl());
    $this->assertSession()->responseContains($user->getDisplayName());
    $tabs = $this->cssSelect("#block-{$block} ul li a");
    $this->assertCount(0, $tabs);
  }

}
