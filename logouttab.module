<?php

/**
 * @file
 * Adds a logout tab to the user page.
 */

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Url;

/**
 * Implements hook_help().
 */
function logouttab_help($route_name) {
  switch ($route_name) {
    case 'help.page.logouttab':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('The module allows user to log out using new tab from the profile page.') . '</p>';
      $output .= '<h3>' . t('Uses') . '</h3>';
      $output .= '<dl>';
      $output .= '<dt>' . t('Configuring the tab settings') . '</dt>';
      $output .= '<dd>' . t('On the <a href=":logouttab-settings">Settings page</a>, you can set the weight of the tab and URL to use for logout.', [
        ':logouttab-settings' => Url::fromRoute('logouttab.settings')->toString(),
      ]) . '</dd>';
      $output .= '</dl>';
      return $output;
  }
}

/**
 * Implements hook_menu_local_tasks_alter().
 *
 * Changes weight and access for the 'Log out' tab on the user page.
 */
function logouttab_menu_local_tasks_alter(&$data, $route_name, &$cacheability) {
  if (isset($data['tabs'][0]['entity.user.logouttab'])) {
    $tab_data = &$data['tabs'][0]['entity.user.logouttab'];
    $link_params = $tab_data['#link']['url']->getRouteParameters();
    $tab_data['#access'] = $link_params['user'] == \Drupal::currentUser()->id();
    if ($tab_data['#access']) {
      // Change tab weight to user configured.
      $config = \Drupal::config('logouttab.settings');
      $tab_data['#weight'] = $config->get('weight');
      // Make cache dependent on config.
      $cacheability = $cacheability->merge(CacheableMetadata::createFromObject($config));
    }
  }
}
